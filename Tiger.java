package pkg2d;

import java.util.Random;


public class Tiger 
{
    private boolean doorLeft;
    private boolean doorRight;
    private String leftIndicator;
    private String rightIndicator;
            
    Random r;
    
    public Tiger()
    {
        r = new Random();
        leftIndicator = "L";
        rightIndicator = "R";
        
        boolean flipCoin = r.nextBoolean();
        if(flipCoin)
        {
            doorLeft = true;
            doorRight = false;
        }
        else
        {
            doorRight = true;
            doorLeft = false;
        }
    }
    
    private String lies()
    {
        if(doorLeft)
        {
            return rightIndicator.intern();
        }
        else
        {
            return leftIndicator.intern();
        }
        
    }
    
    private String truth()
    {
        if(doorRight)
        {
            return rightIndicator.intern();
        }
        else
        {
            return leftIndicator.intern(); 
        }

    }
    
    /**
     * Allows player to listen where the tiger is...
     * 
     * Returns 'L' or 'R' with 85% chance that the returned door is correct
     * @return 
     */
    public String listen()
    {
        float chance = r.nextFloat();
        if(chance > 0.15f)
        {
            return lies();
        }
        else
        {
            return truth();
        }
    }
    
    public boolean open(String door)
    {
        if(door.equals(leftIndicator))
        {
            return doorLeft;
        }
        else
        {
            return doorRight;
        }
    }

}
