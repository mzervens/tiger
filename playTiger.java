

package pkg2d;

import java.util.ArrayList;
import java.util.List;


public class playTiger 
{
    private static int iterations = 100000000;
    
    private static int maxListen = 4;
    
    private static String looking1 [] = {"R"};
    
    private static String looking2 [] = {"R", "R", "R", "L"};
    
    private static List<String []> successResults = new ArrayList<>();
    private static List<String []> failureResults = new ArrayList<>();
    
    private static boolean matcher(String data [])
    {
        if(looking1.length == maxListen)
        {
            for(int i = 0; i < looking1.length; i++)
            {
                if(!(looking1[i].equals(data[i])))
                {
                    return false;
                }
            }
        
            return true;
        }
        else
        {
            for(int i = 0; i < looking2.length; i++)
            {
                if(!(looking2[i].equals(data[i])))
                {
                    return false;
                }
            
            }
            
            return true;
        
        }
    }
    
    public static void printLookupSequence()
    {
        if(looking1.length == maxListen)
        {
            for(int i = 0; i < looking1.length; i++)
            {
                System.out.print(looking1[i]);
            }

        }
        else
        {
            for(int i = 0; i < looking2.length; i++)
            {
                System.out.print(looking2[i]);
            }

        }
    }
    
    public static void main(String args [])
    {
        String listenResults [] = new String[maxListen];
        boolean result;
        
        for(int i = 0; i < iterations; i++)
        {
            Tiger g = new Tiger();
            for(int n = 0; n < maxListen; n++)
            {
                listenResults[n] = g.listen();
            }

            if(matcher(listenResults))
            {
                result = g.open("R");
                
                if(result)
                {
                    failureResults.add(listenResults);
                }
                else
                {
                    successResults.add(listenResults);
                }
            
            }
        
        }
        
        System.out.println("Out of total " + iterations + " games,");
        System.out.println("there were " + (failureResults.size() + successResults.size()) + " games,");
        System.out.print("where occured the following listening sequence ");
        printLookupSequence();
        System.out.println(".");
        
        System.out.println("");
        
        System.out.println("Out of those games,");
        System.out.println(successResults.size() + " games ended by opening the door WITHOUT tiger, but");
        System.out.println(failureResults.size() + " games ended by opening the door WITH tiger.");

    }

}
